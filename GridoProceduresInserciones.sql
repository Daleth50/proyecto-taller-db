-- Inserciones Proveedores --
create proc InsProv
@nomprov varchar(25),
@telprov varchar(25),
@ban int output
as
declare @clveprov int
if (select COUNT(*) from Proveedores)=0
	set @clveprov=1
else
	set @clveprov=(select MAX(clveprov) from Proveedores)+1
if @nomprov=''
	--set @ban=1
	print 'Error: El nombre del proveedor no debe estar vacio.'
	else
		if (select nomprov from Proveedores where nomprov=@nomprov)=@nomprov
			print 'Error: Ese proveedor ya existe'
			--set @ban=2
		else 
			if @telprov=''
				print 'Error: El telefono del proveedor no debe estar vacio.'
				--set @ban=3
			else
				if(select telprov from Proveedores where telprov=@telprov)=@telprov
					print 'Error: Ese telefono ya existe'
					--set @ban=4
				else
					begin
						begin tran 
							insert into Proveedores(nomprov,telprov,clveprov) values (@nomprov, @telprov,@clveprov)
						commit tran
					end

-- Compras --
create procedure InsCompras
@clveprov int,
@ban int output
as
	declare @clvecomp int
	if @clveprov=''
		--set @ban=2
		print 'Error: La clave del proveedor no debe estar vacia.'
	else
		if @clveprov=(select clveprov from Proveedores where clveprov=@clveprov)
			begin
				begin tran
					insert into Compras(totalc) values (0)
				commit tran
				set @clvecomp=(select MAX(clvecomp) from Compras)
				begin tran
					insert into ProvComp(clvecomp,clveprov) values(@clvecomp, @clveprov)
				commit tran
			end
		else
			print 'Error: No se encontro la clave del proveedor'
			--set @ban=1

-- Inserciones Toppings --
create proc InsTopping
@nomtopp varchar(25),
@ban int output
as
if @nomtopp=''
	print 'Error: El nombre del Topping no puede estar vacio'
	--set @ban = 1
else
	if (select edot from Topping where nomt=@nomtopp)=0
	begin
	begin tran
		update Topping set edot=1 where nomt=@nomtopp
	commit tran
	end
	else
		begin
			begin tran
				insert into Topping(nomt) values (@nomtopp)
			commit tran
		end

-- CompTopping --
create proc InsCompTopping
@clvetopp int,
@clvecomp int,
@cantcomp int,
@precio money,
@ban int output
as
if @cantcomp>0
	if (select count(*) from Topping where clvetopp=@clvetopp)=1
		if (select count(*) from Compras where clvecomp=@clvecomp)=1
				if(select COUNT(*) from CompTopping where clvecomp=@clvecomp and clvetopp=@clvetopp)=0
						begin
						begin tran
							update Topping set preciot=@precio, existenciat=existenciat+@cantcomp where clvetopp=@clvetopp
							update Compras set totalc=totalc+(@cantcomp*@precio) where clvecomp=@clvecomp
							insert into CompTopping(clvecomp,clvetopp,cantcomp) values (@clvecomp,@clvetopp,@cantcomp)
						commit tran
						end
				else
					begin 
						begin tran
							update Topping set preciot=@precio, existenciat=existenciat+@cantcomp,edot=1 where clvetopp=@clvetopp
							update Compras set totalc=totalc+(@cantcomp*@precio) where clvecomp=@clvecomp
							update CompTopping set cantcomp= cantcomp+@cantcomp where clvecomp=@clvecomp and clvetopp=@clvetopp
						commit tran
					end			
				else
			print 'Error: La clave de la compra no existe'
			--set @ban=1
	else
		print 'Error: La clave del topping no existe'
		--set @ban=2
else
	print 'Error: La cantidad comprada no puede ser 0'
	--set @ban=3

-- Inserciones Sabores --
create proc InsSabor
@nomsab varchar(25),
@ban int output
as
if @nomsab=''
	print 'Error: El nombre del Sabor no puede estar vacio'
	--set @ban = 1
else
if (select edo from Sabor where nom=@nomsab)=0
	begin
	begin tran
		update Sabor set edo=1 where nom=@nomsab
	commit tran
	end
	else
	begin
		begin tran
			insert into Sabor(nom) values (@nomsab)
		commit tran
	end

-- CompSabor --
create proc InsCompSabor
@clvesabor int,
@clvecomp int,
@cantcomp int,
@precio money,
@ban int output
as
if @cantcomp>0
	if (select count(*) from Sabor where clvesabor=@clvesabor)=1
		if (select count(*) from Compras where clvecomp=@clvecomp)=1
					if(select COUNT(*) from CompSabor where clvecomp=@clvecomp and clvesabor=@clvesabor)=0
					begin 
					begin tran
							update Sabor set precio=@precio, existencia=existencia+@cantcomp where clvesabor=@clvesabor
							update Compras set totalc=totalc+(@cantcomp*@precio) where clvecomp=@clvecomp
							insert into CompSabor(clvecomp,clvesabor,cantcomp) values (@clvecomp,@clvesabor,@cantcomp)
						commit tran
					end
				else
					begin 
						begin tran
							update Sabor set precio=@precio, existencia=existencia+@cantcomp,edo=1 where clvesabor=@clvesabor
							update Compras set totalc=totalc+(@cantcomp*@precio) where clvecomp=@clvecomp
							update CompSabor set cantcomp= cantcomp+@cantcomp where clvecomp=@clvecomp and clvesabor=@clvesabor
						commit tran
					end
		else
			print 'Error: La clave de la compra no existe'
			--set @ban=1
	else
		print 'Error: La clave del sabor no existe'
		--set @ban=2
else
	print 'Error: La cantidad comprada no puede ser 0'
	--set @ban=3

-- Insertar Helado --
create proc InsHelado
@precioh money,
@tamano varchar(15),
@presentacion varchar(15),
@ban int output
as
declare @clvehel int
	if (select Count(clvehel) from Helados)>0
		set @clvehel=(select MAX(clvehel) from Helados)+1
	else
		set @clvehel=1
	if @precioh=0
		print 'Error: El precio del helado no puede ser 0'
		--set @ban=1
	else
		if @tamano=''
			print 'Error: El tama�o no puede ir vacio'
			--set @ban=2
		else 
			if @presentacion=''
				print 'Error: La presentacion no puede estar vacia'
				--set @ban=3
			else
				begin
					begin tran
						insert into Helados values (@precioh,@tamano,@presentacion,@clvehel)
					commit tran
				end

-- ToppHelado --
create proc InsToppHelado
@canttop int,
@clvetopp int,
@clvehel int,
@ban int output
as
if @clvehel =(Select clvehel from Helados where clvehel=@clvehel)
	if @clvetopp = (Select clvetopp from Topping where clvetopp=@clvetopp)
		if(select edot from Topping where clvetopp=@clvetopp)=1
			if (select existenciat from Topping where clvetopp=@clvetopp)>=@canttop
				begin
				if(select COUNT(*) from ToppHelado where clvehel=@clvehel and clvetopp=@clvetopp)=0
				begin
				begin tran
						insert into ToppHelado values (@canttop,@clvetopp,@clvehel)
						update Topping set existenciat=existenciat-@canttop where clvetopp=@clvetopp
						update Helados set precioh=precioh+((select preciot from Topping where clvetopp=@clvetopp)*@canttop) where clvehel=@clvehel
					commit tran 
				end
				else
				begin
				begin tran
						update ToppHelado set canttop= canttop+@canttop where clvehel=@clvehel and clvetopp=@clvetopp
						update Topping set existenciat=existenciat-@canttop where clvetopp=@clvetopp
						update Helados set precioh=precioh+((select preciot from Topping where clvetopp=@clvetopp)*@canttop) where clvehel=@clvehel
					commit tran 
				end
					
				end 
			else
			print 'Error: No quedan suficientes unidades de ese Topping'
			--set @ban=3
		else
			print 'Error: El Topping esta deshabilitado.'
			--set @ban=4
	else
	print 'Error: Clave del Topping no existe'
	--set @ban=2
else
	print 'Error: Clave del Helado no existe'	
	--set @ban=1

-- SaborHelado --
create proc InsSaborHelado
@cantsabor int,
@clvesabor int,
@clvehel int,
@ban int output
as
if @clvehel =(Select clvehel from Helados where clvehel=@clvehel)
	if @clvesabor = (Select clvesabor from Sabor where clvesabor=@clvesabor)
		if(select edo from Sabor where clvesabor=@clvesabor)=1
			if (select existenciat from Topping where clvetopp=@clvesabor)>=@cantsabor
				begin
				if(select COUNT(*) from SaborHelado where clvehel=@clvehel and clvesabor=@clvesabor)=0
				begin 
				begin tran
						insert into SaborHelado values (@cantsabor,@clvesabor,@clvehel)
						update Sabor set existencia=existencia-@cantsabor where clvesabor=@clvesabor
						update Helados set precioh=precioh+((select precio from Sabor where clvesabor=@clvesabor)*@clvesabor) where clvehel=@clvehel
					commit tran 
				end
				else
				begin
				begin tran
						update SaborHelado set cantsabor= cantsabor+@cantsabor where clvehel=@clvehel and clvesabor=@clvesabor
						update Sabor set existencia=existencia-@cantsabor where clvesabor=@clvesabor
						update Helados set precioh=precioh+((select precio from Sabor where clvesabor=@clvesabor)*@clvesabor) where clvehel=@clvehel
					commit tran 
				end
					
				end 
			else
			print 'Error: No quedan suficientes unidades de ese Sabor'
			--set @ban=3
		else
			print 'Error: El Sabor esta deshabilitado.'
			--set @ban=4
	else
	print 'Error: Clave del Sabor no existe'
	--set @ban=2
else
	print 'Error: Clave del Helado no existe'	
	--set @ban=1

-- Ventas --
create proc InsVenta
@ban int output
as
declare @clvevent int
if (select COUNT(clvevent) from Ventas)>0
	set @clvevent=(select MAX(clvevent) from Ventas)+1
else
	set @clvevent=1
begin tran
	insert into Ventas (clvevent) values (@clvevent)
commit tran

--VentaHelado--
create proc InsVentHel
@clvevent int,
@clvehel int,
@cantvend int,
@ban int output
as
declare @totalv money
declare @iva float
declare @subtotal money
if @cantvend <= 0
print 'Error: La cantidad no puede ser 0'
--set @ban= 3
else
if(select clvehel from Helados where clvehel=@clvehel)=@clvehel
	if(select clvevent from Ventas where clvevent=@clvevent)=@clvevent
			if(select count(*) from HelVenta where clvevent=@clvevent and clvehel=@clvehel)=0
			begin
				set @totalv= @cantvend * (select precioh From Helados where clvehel=@clvehel)
				set @totalv = @totalv*1.16
				set @subtotal=@totalv/1.16
				set @iva = @subtotal * .16
				begin tran
					insert into HelVenta values(@cantvend,@clvehel,@clvevent)
					update Ventas set totalv=totalv+@totalv where clvevent=@clvevent
					update Ventas set subtotal=subtotal+@subtotal where clvevent=@clvevent
					update Ventas set iva=iva+@iva where clvevent=@clvevent
				commit tran
			end
		else
			begin
				set @totalv= @cantvend * (select precioh From Helados where clvehel=@clvehel)
				set @totalv = @totalv*1.16
				set @subtotal=@totalv/1.16
				set @iva = @subtotal * .16
				begin tran
					update HelVenta set cantvend=cantvend+@cantvend where clvevent=@clvevent and clvehel=@clvehel
					update Ventas set totalv=totalv+@totalv where clvevent=@clvevent
					update Ventas set subtotal=subtotal+@subtotal where clvevent=@clvevent
					update Ventas set iva=iva+@iva where clvevent=@clvevent
				commit tran
			end
	else
		print 'Error: Esa clave de la venta no existe'
		 --set @ban=1
else
	print 'Error: La clave del helado no existe'
	--set @ban=2