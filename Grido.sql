use master
drop database Grido
create database Grido

use Grido

create table Proveedores
(
nomprov varchar(25) not null,
telprov varchar(25) not null,
clveprov int primary key 
)

create table Compras 
(
fechac date default getdate(),
totalc money default (0),
clvecomp int primary key identity(1,1)
)

create table ProvComp
(
clveprov int foreign key references Proveedores,
clvecomp int foreign key references Compras
)

create table Topping
(
preciot money default (0),
existenciat int default (0),
edot int default(1),
nomt varchar(25),
clvetopp int primary key identity(1,1)
)

create table CompTopping
(
cantcomp int not null,
clvecomp int foreign key references Compras,
clvetopp int foreign key references Topping
)


create table Sabor
(
precio money default (0),
existencia int default (0),
edo int default(1),
nom varchar(25),
clvesabor int primary key identity(1,1)
)

create table CompSabor
(
cantcomp int not null,
clvecomp int foreign key references Compras,
clvesabor int foreign key references Sabor
)

create table Helados 
(
precioh money default (0) not null,
tamano varchar(15) not null,
presentacion varchar(15) not null,
clvehel int primary key 
)

create table ToppHelado
(
canttop int not null,
clvetopp int foreign key references Topping,
clvehel int foreign key references Helados
)

create table SaborHelado
(
cantsabor int not null,
clvesabor int foreign key references Sabor,
clvehel int foreign key references Helados
)

create table Ventas 
(
fechav date default (getdate()) not null,
totalv money default (0) not null,
subtotal money default (0) not null,
iva money default (0) not null,
clvevent int primary key 
) 

create table HelVenta
(
cantvend int not null,
clvehel int foreign key references Helados,
clvevent int foreign key references Ventas
)
