use Grido

-- Eliminaciones a Proveedores
create proc elimprov
@clveprov int,
@ban int output
as
if @clveprov=''
 print 'No pusiste la clave del proveedor'
--set @ban=1
else
	if 1 =(select count(*) from Proveedores where clveprov=@clveprov)
		if 0 =(select count(*) from ProvComp where clveprov=@clveprov)
			begin
				begin tran 
					delete Proveedores where clveprov=@clveprov
				commit tran
			end
		else
			begin
				 print 'Error: no se puede eliminar el proveedor, tiene registros en compras'
				--set @ban=3 
			end
	else
	 print 'La clave del proveedor no existe'
	--set @ban=2 

--Eliminar compra
create  proc elimcompra
@clvecomp int,
@ban int output
as
declare @fechav date
declare @fecha date
declare @clvtopp int
declare @clvsabor int
declare @canttop int
declare @cantsab int
declare @Sabor varchar(10) = 'Sabor'
declare @Topping varchar(10)= 'Topping'
if @clvecomp = ''
begin
	print 'La clave de la compra esta vacia.'
	--set @ban=1
	return
end
if(select count(*) from Compras where clvecomp=@clvecomp) = 0
begin
	print 'Error: La clave de compra no existe'
	--set @ban=3
	return
end
set @fechav= (select fechac from Compras where clvecomp=@clvecomp)
set @fecha = GETDATE()
if(DATEDIFF(day,@fechav,@fecha) != 0)
begin
	print 'Error: la venta no puede ser eliminada'
	--set @ban=2
	return
end
if(select count(*) from CompTopping where clvecomp=@clvecomp) = 1
	begin
		set @canttop=(select cantcomp from CompTopping where clvecomp=@clvecomp)
		set @clvtopp=(select clvetopp from CompTopping where clvecomp=@clvecomp)
		print @canttop
		if(select existenciat from Topping where clvetopp=@clvtopp) >= @canttop
		begin
		begin tran @Topping
		update Topping set existenciat= existenciat-@canttop where clvetopp=@clvtopp
		delete CompTopping where clvecomp=@clvecomp
		commit tran
		end
		else
		begin
		print 'Error: no hay suficientes toppings en stock'
		--set @ban =4
		return
		end		
end
if(select count(*) from CompSabor where clvecomp=@clvecomp) = 1
begin
		set @clvsabor=(select clvesabor from CompSabor where clvecomp=@clvecomp)
		set @cantsab=(select cantcomp from CompSabor where clvecomp=@clvecomp)
		print @cantsab
		if(select existencia from Sabor where clvesabor=@clvsabor)>= @cantsab
		begin
		begin tran @Sabor
		update Sabor set existencia = existencia-@cantsab where clvesabor=@clvsabor
		delete CompSabor where clvecomp=@clvecomp
		commit tran
		end
		else
		begin
			print 'Error: no queda suficiente sabor en stock'
		--set @ban =5
		rollback tran @Topping
		return
		end
end
if(select count(*) from CompTopping where clvecomp=@clvecomp) = 0
begin
	if(select count(*) from CompSabor where clvecomp=@clvecomp) = 0
	begin
		begin tran
		delete ProvComp where clvecomp=@clvecomp
		delete Compras where clvecomp=@clvecomp
		commit tran
	end
end

--eliminar sabor --
create proc elimSabor
@clvesabor int,
@ban int output
as 
if(@clvesabor  = '')
begin
	print 'Error: la clave de sabor no puede estar vacia'
	--set @ban=1
end
else
if(select clvesabor from Sabor where clvesabor=@clvesabor)=@clvesabor
begin
		if(select existencia from Sabor where clvesabor=@clvesabor) < 1
		begin
			update Sabor set edo=0 where clvesabor=@clvesabor
		end
		else
		begin
		print 'Error: El sabor aun tiene existencias'
		--set @ban= 2
	end
end
else
print 'Error: la clave de sabor no existe'
--set ban= 3

--eliminar topping --
create proc elimTopp
@clvetopp int,
@ban int output
as 
if(@clvetopp  = '')
begin
	print 'Error: la clave de sabor no puede estar vacia'
	--set @ban=1
end
else
if(select clvetopp from Topping where clvetopp=@clvetopp)=@clvetopp
begin
	if(select existenciat from Topping where clvetopp=@clvetopp) < 1
		begin
			update Topping set edot=0 where clvetopp=@clvetopp
		end
		else
		begin
		print 'Error: El topping aun tiene existencias'
		--set @ban= 2
	end
end
else
print 'Error: la clave de topping no existe'
--set ban= 3

--eliminar helado--
create proc eliminaHel
@clvhel int,
@ban int output
as
declare @cant int
declare @clvesabor int
declare @cuantos int

if @clvhel = ''
	print 'La clave del helado esta vacia.'
	--set @ban=1
else
if(select count(*) from HelVenta where clvehel=@clvhel)!= 0
begin
	print 'Error: hay una venta relacionada a ese helado'
	return
end
set @cant=(select cantsabor from SaborHelado where clvehel=@clvhel)	
	if(select clvehel from Helados where clvehel=@clvhel)=@clvhel
		begin
			set @cuantos=(select count(*) from SaborHelado where clvehel=@clvhel)
			while @cuantos>0
				begin
					set @clvesabor=(select max(clvesabor) from SaborHelado where clvehel=@clvhel)
						update Sabor set existencia=existencia+@cant where clvesabor=@clvesabor
						delete SaborHelado where clvehel=@clvhel						
						set @cuantos=@cuantos-1
					
				end
			if(select count(*) from SaborHelado where clvehel=@clvhel)=0
			begin
				begin tran
					exec eliminaHelTopp @clvhel,0
				commit tran
			end
		end
	else
		print 'La clave del helado no existe.'
		--set @ban=2

create proc eliminaHelTopp
@clvhel int,
@ban int output
as
declare @cant int
declare @clvetopp int
declare @cuantos int

if @clvhel = ''
	--print 'La clave de la compra esta vacia.'
	set @ban=1
else
set @cant=(select canttop from ToppHelado where clvehel=@clvhel)	
	if(select clvehel from Helados where clvehel=@clvhel)=@clvhel
		begin
			set @cuantos=(select count(*) from ToppHelado where clvehel=@clvhel)
			while @cuantos>0
				begin
					set @clvetopp=(select max(clvetopp) from ToppHelado where clvehel=@clvhel)
						update Topping set existenciat=existenciat+@cant where clvetopp=@clvetopp
						delete ToppHelado where clvehel=@clvhel						
						set @cuantos=@cuantos-1
				end
			if(select count(*) from ToppHelado where clvehel=@clvhel)=0
			begin
				begin tran
					delete Helados where clvehel=@clvhel
				commit tran
			end
		end
	else
		print 'La clave de la compra no existe.'
		--set @ban=2
return

--eliminar ventas--
create  proc elimVenta
@clvevent int,
@ban int output
as
declare @fechav date
declare @fecha date
if @clvevent = ''
begin
	print 'Error: la clave de venta no puede estar vacia'
	--set @ban=1
	return
end
if(select COUNT(*) from Ventas where clvevent=@clvevent) = 0
begin
	print 'Error: la clave de venta no existe'
	--set @ban=3
	return
end
set @fechav= (select fechav from Ventas where clvevent=@clvevent)
set @fecha = GETDATE()
if(DATEDIFF(day,@fechav,@fecha) != 0)
begin
	print 'Error: la venta no puede ser eliminada'
	--set @ban=2
	return
end
begin
	begin tran
	delete HelVenta where clvevent=@clvevent
	delete Ventas where clvevent=@clvevent
	commit tran
end
