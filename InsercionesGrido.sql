
use Grido
--inserciones provedor --
exec InsProv '','3535367771',0
exec InsProv 'Pedro','',0
exec InsProv 'Pedro','3535367771',0
exec InsProv 'Pedro Daleth','3535367771',0
exec InsProv 'Pedro Daleth','3535367775',0
select * from Proveedores

--inserciones compras--
exec InsCompras '',0
exec InsCompras 1000,0
exec InsCompras 1,0
exec InsCompras 2,0
select * from Compras
select * from ProvComp

--inserciones toppings--
exec InsTopping '',0
exec InsTopping 'chocolate',0
exec InsTopping 'gomitas',0
update Topping set edot=0 where nomt='gomitas'
select * from Topping
--inserciones topping compra
exec InsCompTopping 100,1,100,100,0 --clve topping
exec InsCompTopping 3,100,100,100,0 --clve compras
exec InsCompTopping 3,100,0,100,0   --cantidad 0
exec InsCompTopping 1,4,100,5,0 
select * from CompTopping
select * from Compras
select * from Topping
delete CompTopping where clvecomp=2
--inserciones sabores---
exec InsSabor '',0
exec InsSabor 'vainilla',0
exec InsSabor 'fresa',0
update Sabor set edo= 0 where nom='fresa'
select * from Sabor
--inserciones sabores compra
exec InsCompSabor 100,1,100,50,0 --la clave de sabor no existe
exec InsCompSabor 3,100,100,50,0 --la clave de compra no existe
exec InsCompSabor 3,1,0,50,0 --la cantidad es 0
exec InsCompSabor 1,4,200,1,0
select * from Compras
select * from Sabor
select * from CompSabor
--inserciones helado--
exec InsHelado 0,'chico','vaso',0 --precio 
exec InsHelado 20,'','vaso',0 --tama�o
exec InsHelado 20,'chico','',0 --presentacion
exec InsHelado 5,'chico','vaso',0
exec InsHelado 20,'mediano','vaso',0
exec InsHelado 30,'mediano','cono',0
select * from Helados

--inserciones topping helado---
exec InsToppHelado 10,100,1,0 --clave de topping no existe
exec InsToppHelado 10,1,100,0 --clave del helado no existe
exec InsToppHelado 100000,3,1,0 --sin stock suficiente
exec InsToppHelado 2,1,1,0
select * from ToppHelado
select * from Topping
select * from Helados

--inserciones sabor helado--
exec InsSaborHelado 10,100,1,0
exec InsSaborHelado 10,1,100,0
exec InsSaborHelado 10000,3,1,0
exec InsSaborHelado 1,1,1,0
select * from Sabor
select * from SaborHelado
select * from Helados
--inserciones ventas---
exec InsVenta 0
select * from Ventas
--insercion venta helado--
exec InsVentHel 1000,1,10,0 --clave de venta no existe
exec InsVentHel 1,10000,10,0--clave del helado no existe
exec InsVentHel 1,1,0,0 --cantidad 0
exec InsVentHel 1,1,1,0
select * from Helados
select * from Ventas
select * from HelVenta




