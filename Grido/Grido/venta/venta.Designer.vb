﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class venta
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.sabores = New System.Windows.Forms.ComboBox()
        Me.SaborBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridoDataSet = New Grido.GridoDataSet()
        Me.SaborBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.HeladosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.HeladosTableAdapter = New Grido.GridoDataSetTableAdapters.HeladosTableAdapter()
        Me.SaborTableAdapter = New Grido.GridoDataSetTableAdapters.SaborTableAdapter()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.ToppingBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ToppingTableAdapter = New Grido.GridoDataSetTableAdapters.ToppingTableAdapter()
        Me.FillByToolStrip = New System.Windows.Forms.ToolStrip()
        Me.FillByToolStripButton = New System.Windows.Forms.ToolStripButton()
        CType(Me.SaborBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridoDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SaborBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.HeladosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ToppingBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.FillByToolStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(58, 51)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(104, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Selecciona un sabor"
        '
        'sabores
        '
        Me.sabores.DataSource = Me.SaborBindingSource
        Me.sabores.DisplayMember = "nom"
        Me.sabores.FormattingEnabled = True
        Me.sabores.Location = New System.Drawing.Point(61, 78)
        Me.sabores.Name = "sabores"
        Me.sabores.Size = New System.Drawing.Size(121, 21)
        Me.sabores.TabIndex = 1
        Me.sabores.ValueMember = "clvesabor"
        '
        'SaborBindingSource
        '
        Me.SaborBindingSource.DataMember = "Sabor"
        Me.SaborBindingSource.DataSource = Me.GridoDataSet
        '
        'GridoDataSet
        '
        Me.GridoDataSet.DataSetName = "GridoDataSet"
        Me.GridoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SaborBindingSource1
        '
        Me.SaborBindingSource1.DataMember = "Sabor"
        Me.SaborBindingSource1.DataSource = Me.GridoDataSet
        '
        'HeladosBindingSource
        '
        Me.HeladosBindingSource.DataMember = "Helados"
        Me.HeladosBindingSource.DataSource = Me.GridoDataSet
        '
        'HeladosTableAdapter
        '
        Me.HeladosTableAdapter.ClearBeforeFill = True
        '
        'SaborTableAdapter
        '
        Me.SaborTableAdapter.ClearBeforeFill = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(264, 50)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Selecciona toppings"
        '
        'ListBox1
        '
        Me.ListBox1.DataSource = Me.ToppingBindingSource
        Me.ListBox1.DisplayMember = "nomt"
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.Location = New System.Drawing.Point(267, 78)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(120, 95)
        Me.ListBox1.TabIndex = 3
        Me.ListBox1.ValueMember = "clvetopp"
        '
        'ToppingBindingSource
        '
        Me.ToppingBindingSource.DataMember = "Topping"
        Me.ToppingBindingSource.DataSource = Me.GridoDataSet
        '
        'ToppingTableAdapter
        '
        Me.ToppingTableAdapter.ClearBeforeFill = True
        '
        'FillByToolStrip
        '
        Me.FillByToolStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.FillByToolStripButton})
        Me.FillByToolStrip.Location = New System.Drawing.Point(0, 0)
        Me.FillByToolStrip.Name = "FillByToolStrip"
        Me.FillByToolStrip.Size = New System.Drawing.Size(800, 25)
        Me.FillByToolStrip.TabIndex = 4
        Me.FillByToolStrip.Text = "FillByToolStrip"
        '
        'FillByToolStripButton
        '
        Me.FillByToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text
        Me.FillByToolStripButton.Name = "FillByToolStripButton"
        Me.FillByToolStripButton.Size = New System.Drawing.Size(39, 22)
        Me.FillByToolStripButton.Text = "FillBy"
        '
        'venta
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.FillByToolStrip)
        Me.Controls.Add(Me.ListBox1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.sabores)
        Me.Controls.Add(Me.Label1)
        Me.Name = "venta"
        Me.Text = "Crear Venta"
        CType(Me.SaborBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridoDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SaborBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.HeladosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ToppingBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.FillByToolStrip.ResumeLayout(False)
        Me.FillByToolStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents sabores As ComboBox
    Friend WithEvents GridoDataSet As GridoDataSet
    Friend WithEvents HeladosBindingSource As BindingSource
    Friend WithEvents HeladosTableAdapter As GridoDataSetTableAdapters.HeladosTableAdapter
    Friend WithEvents SaborBindingSource As BindingSource
    Friend WithEvents SaborTableAdapter As GridoDataSetTableAdapters.SaborTableAdapter
    Friend WithEvents SaborBindingSource1 As BindingSource
    Friend WithEvents Label2 As Label
    Friend WithEvents ListBox1 As ListBox
    Friend WithEvents ToppingBindingSource As BindingSource
    Friend WithEvents ToppingTableAdapter As GridoDataSetTableAdapters.ToppingTableAdapter
    Friend WithEvents FillByToolStrip As ToolStrip
    Friend WithEvents FillByToolStripButton As ToolStripButton
End Class
