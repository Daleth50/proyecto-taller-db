﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class mn
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.PreveedoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AltasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.BajasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificacionesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ComprasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AltasToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.BajasToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificacionesToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ProductosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AltasToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.BajasToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificacionesToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem4 = New System.Windows.Forms.ToolStripMenuItem()
        Me.SalirToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FontDialog1 = New System.Windows.Forms.FontDialog()
        Me.crearVenta = New System.Windows.Forms.Button()
        Me.MenuStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.PreveedoresToolStripMenuItem, Me.ComprasToolStripMenuItem, Me.ProductosToolStripMenuItem, Me.ToolStripMenuItem1, Me.SalirToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(800, 24)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        'PreveedoresToolStripMenuItem
        '
        Me.PreveedoresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AltasToolStripMenuItem, Me.BajasToolStripMenuItem, Me.ModificacionesToolStripMenuItem})
        Me.PreveedoresToolStripMenuItem.Name = "PreveedoresToolStripMenuItem"
        Me.PreveedoresToolStripMenuItem.Size = New System.Drawing.Size(83, 20)
        Me.PreveedoresToolStripMenuItem.Text = "Preveedores"
        '
        'AltasToolStripMenuItem
        '
        Me.AltasToolStripMenuItem.Name = "AltasToolStripMenuItem"
        Me.AltasToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.AltasToolStripMenuItem.Text = "Altas"
        '
        'BajasToolStripMenuItem
        '
        Me.BajasToolStripMenuItem.Name = "BajasToolStripMenuItem"
        Me.BajasToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.BajasToolStripMenuItem.Text = "Bajas"
        '
        'ModificacionesToolStripMenuItem
        '
        Me.ModificacionesToolStripMenuItem.Name = "ModificacionesToolStripMenuItem"
        Me.ModificacionesToolStripMenuItem.Size = New System.Drawing.Size(180, 22)
        Me.ModificacionesToolStripMenuItem.Text = "Modificaciones"
        '
        'ComprasToolStripMenuItem
        '
        Me.ComprasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AltasToolStripMenuItem4, Me.BajasToolStripMenuItem4, Me.ModificacionesToolStripMenuItem4})
        Me.ComprasToolStripMenuItem.Name = "ComprasToolStripMenuItem"
        Me.ComprasToolStripMenuItem.Size = New System.Drawing.Size(67, 20)
        Me.ComprasToolStripMenuItem.Text = "Compras"
        '
        'AltasToolStripMenuItem4
        '
        Me.AltasToolStripMenuItem4.Name = "AltasToolStripMenuItem4"
        Me.AltasToolStripMenuItem4.Size = New System.Drawing.Size(180, 22)
        Me.AltasToolStripMenuItem4.Text = "Nueva compra"
        '
        'BajasToolStripMenuItem4
        '
        Me.BajasToolStripMenuItem4.Name = "BajasToolStripMenuItem4"
        Me.BajasToolStripMenuItem4.Size = New System.Drawing.Size(180, 22)
        Me.BajasToolStripMenuItem4.Text = "Bajas"
        '
        'ModificacionesToolStripMenuItem4
        '
        Me.ModificacionesToolStripMenuItem4.Name = "ModificacionesToolStripMenuItem4"
        Me.ModificacionesToolStripMenuItem4.Size = New System.Drawing.Size(180, 22)
        Me.ModificacionesToolStripMenuItem4.Text = "Modificaciones"
        '
        'ProductosToolStripMenuItem
        '
        Me.ProductosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AltasToolStripMenuItem3, Me.BajasToolStripMenuItem3, Me.ModificacionesToolStripMenuItem3})
        Me.ProductosToolStripMenuItem.Name = "ProductosToolStripMenuItem"
        Me.ProductosToolStripMenuItem.Size = New System.Drawing.Size(60, 20)
        Me.ProductosToolStripMenuItem.Text = "Sabores"
        '
        'AltasToolStripMenuItem3
        '
        Me.AltasToolStripMenuItem3.Name = "AltasToolStripMenuItem3"
        Me.AltasToolStripMenuItem3.Size = New System.Drawing.Size(155, 22)
        Me.AltasToolStripMenuItem3.Text = "Altas"
        '
        'BajasToolStripMenuItem3
        '
        Me.BajasToolStripMenuItem3.Name = "BajasToolStripMenuItem3"
        Me.BajasToolStripMenuItem3.Size = New System.Drawing.Size(155, 22)
        Me.BajasToolStripMenuItem3.Text = "Bajas"
        '
        'ModificacionesToolStripMenuItem3
        '
        Me.ModificacionesToolStripMenuItem3.Name = "ModificacionesToolStripMenuItem3"
        Me.ModificacionesToolStripMenuItem3.Size = New System.Drawing.Size(155, 22)
        Me.ModificacionesToolStripMenuItem3.Text = "Modificaciones"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem2, Me.ToolStripMenuItem3, Me.ToolStripMenuItem4})
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(63, 20)
        Me.ToolStripMenuItem1.Text = "Topping"
        '
        'ToolStripMenuItem2
        '
        Me.ToolStripMenuItem2.Name = "ToolStripMenuItem2"
        Me.ToolStripMenuItem2.Size = New System.Drawing.Size(155, 22)
        Me.ToolStripMenuItem2.Text = "Altas"
        '
        'ToolStripMenuItem3
        '
        Me.ToolStripMenuItem3.Name = "ToolStripMenuItem3"
        Me.ToolStripMenuItem3.Size = New System.Drawing.Size(155, 22)
        Me.ToolStripMenuItem3.Text = "Bajas"
        '
        'ToolStripMenuItem4
        '
        Me.ToolStripMenuItem4.Name = "ToolStripMenuItem4"
        Me.ToolStripMenuItem4.Size = New System.Drawing.Size(155, 22)
        Me.ToolStripMenuItem4.Text = "Modificaciones"
        '
        'SalirToolStripMenuItem
        '
        Me.SalirToolStripMenuItem.Name = "SalirToolStripMenuItem"
        Me.SalirToolStripMenuItem.Size = New System.Drawing.Size(41, 20)
        Me.SalirToolStripMenuItem.Text = "Salir"
        '
        'crearVenta
        '
        Me.crearVenta.Location = New System.Drawing.Point(302, 337)
        Me.crearVenta.Name = "crearVenta"
        Me.crearVenta.Size = New System.Drawing.Size(229, 78)
        Me.crearVenta.TabIndex = 1
        Me.crearVenta.Text = "Crear Venta"
        Me.crearVenta.UseVisualStyleBackColor = True
        '
        'mn
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.crearVenta)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "mn"
        Me.Text = "menu"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents PreveedoresToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ProductosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ComprasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SalirToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents FontDialog1 As FontDialog
    Friend WithEvents AltasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents BajasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ModificacionesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AltasToolStripMenuItem3 As ToolStripMenuItem
    Friend WithEvents BajasToolStripMenuItem3 As ToolStripMenuItem
    Friend WithEvents ModificacionesToolStripMenuItem3 As ToolStripMenuItem
    Friend WithEvents AltasToolStripMenuItem4 As ToolStripMenuItem
    Friend WithEvents BajasToolStripMenuItem4 As ToolStripMenuItem
    Friend WithEvents ModificacionesToolStripMenuItem4 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem3 As ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem4 As ToolStripMenuItem
    Friend WithEvents crearVenta As Button
End Class
