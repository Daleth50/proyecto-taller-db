﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class proveedoresbaja
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.GridoDataSet = New Grido.GridoDataSet()
        Me.ProveedoresBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ProveedoresTableAdapter = New Grido.GridoDataSetTableAdapters.ProveedoresTableAdapter()
        Me.btncancelar = New System.Windows.Forms.Button()
        Me.btnbaja = New System.Windows.Forms.Button()
        CType(Me.GridoDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProveedoresBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(162, 29)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(432, 57)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Selecciona el proveedor que desea dar de baja"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.ProveedoresBindingSource
        Me.ComboBox1.DisplayMember = "nomprov"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(275, 89)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(196, 21)
        Me.ComboBox1.TabIndex = 1
        Me.ComboBox1.ValueMember = "clveprov"
        '
        'GridoDataSet
        '
        Me.GridoDataSet.DataSetName = "GridoDataSet"
        Me.GridoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ProveedoresBindingSource
        '
        Me.ProveedoresBindingSource.DataMember = "Proveedores"
        Me.ProveedoresBindingSource.DataSource = Me.GridoDataSet
        '
        'ProveedoresTableAdapter
        '
        Me.ProveedoresTableAdapter.ClearBeforeFill = True
        '
        'btncancelar
        '
        Me.btncancelar.Location = New System.Drawing.Point(166, 251)
        Me.btncancelar.Name = "btncancelar"
        Me.btncancelar.Size = New System.Drawing.Size(167, 66)
        Me.btncancelar.TabIndex = 2
        Me.btncancelar.Text = "cancelar"
        Me.btncancelar.UseVisualStyleBackColor = True
        '
        'btnbaja
        '
        Me.btnbaja.Location = New System.Drawing.Point(427, 251)
        Me.btnbaja.Name = "btnbaja"
        Me.btnbaja.Size = New System.Drawing.Size(167, 66)
        Me.btnbaja.TabIndex = 3
        Me.btnbaja.Text = "Dar de baja"
        Me.btnbaja.UseVisualStyleBackColor = True
        '
        'proveedoresbaja
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.btnbaja)
        Me.Controls.Add(Me.btncancelar)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "proveedoresbaja"
        Me.Text = "proveedoresbaja"
        CType(Me.GridoDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProveedoresBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents GridoDataSet As GridoDataSet
    Friend WithEvents ProveedoresBindingSource As BindingSource
    Friend WithEvents ProveedoresTableAdapter As GridoDataSetTableAdapters.ProveedoresTableAdapter
    Friend WithEvents btncancelar As Button
    Friend WithEvents btnbaja As Button
End Class
