﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class compSabor
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.qtySabor = New System.Windows.Forms.TextBox()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.SaborBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridoDataSet = New Grido.GridoDataSet()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.SaborTableAdapter = New Grido.GridoDataSetTableAdapters.SaborTableAdapter()
        Me.Button3 = New System.Windows.Forms.Button()
        CType(Me.SaborBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridoDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(244, 149)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(295, 25)
        Me.Label2.TabIndex = 11
        Me.Label2.Text = "Ingresa la cantidad comprada"
        Me.Label2.Visible = False
        '
        'qtySabor
        '
        Me.qtySabor.Location = New System.Drawing.Point(275, 196)
        Me.qtySabor.Name = "qtySabor"
        Me.qtySabor.Size = New System.Drawing.Size(215, 20)
        Me.qtySabor.TabIndex = 10
        Me.qtySabor.Visible = False
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(12, 394)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(178, 44)
        Me.Button2.TabIndex = 9
        Me.Button2.Text = "Registrar Sabor"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(286, 309)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(204, 55)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "Comprar Sabor"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.SaborBindingSource
        Me.ComboBox1.DisplayMember = "nom"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(275, 82)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(215, 21)
        Me.ComboBox1.TabIndex = 7
        Me.ComboBox1.ValueMember = "clvesabor"
        '
        'SaborBindingSource
        '
        Me.SaborBindingSource.DataMember = "Sabor"
        Me.SaborBindingSource.DataSource = Me.GridoDataSet
        '
        'GridoDataSet
        '
        Me.GridoDataSet.DataSetName = "GridoDataSet"
        Me.GridoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 15.75!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(272, 44)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(201, 25)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Selecciona el sabor"
        '
        'SaborTableAdapter
        '
        Me.SaborTableAdapter.ClearBeforeFill = True
        '
        'Button3
        '
        Me.Button3.BackColor = System.Drawing.Color.OrangeRed
        Me.Button3.ForeColor = System.Drawing.Color.Transparent
        Me.Button3.Location = New System.Drawing.Point(58, 314)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(178, 44)
        Me.Button3.TabIndex = 12
        Me.Button3.Text = "Cancelar"
        Me.Button3.UseVisualStyleBackColor = False
        '
        'compSabor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 458)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.qtySabor)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "compSabor"
        Me.Text = "compSabor"
        CType(Me.SaborBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridoDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label2 As Label
    Friend WithEvents qtySabor As TextBox
    Friend WithEvents Button2 As Button
    Friend WithEvents Button1 As Button
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents Label1 As Label
    Friend WithEvents GridoDataSet As GridoDataSet
    Friend WithEvents SaborBindingSource As BindingSource
    Friend WithEvents SaborTableAdapter As GridoDataSetTableAdapters.SaborTableAdapter
    Friend WithEvents Button3 As Button
End Class
