﻿Imports System.Text.RegularExpressions

Public Class compraTopping
    Dim clveprov = compras.ComboBox1.SelectedValue
    Private Sub compraTopping_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.ToppingTableAdapter.Fill(Me.GridoDataSet.Topping)
        Label2.Hide()
        qtyTopp.Hide()
        MsgBox(clveprov)
        Try
            Me.ToppingTableAdapter.FillBy1(Me.GridoDataSet.Topping)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        Label2.Show()
        qtyTopp.Show()
    End Sub

    Private Sub qtyTopp_TextChanged(sender As Object, e As EventArgs) Handles qtyTopp.TextChanged
        Dim digitsOnly As Regex = New Regex("[^\d]")
        qtyTopp.Text = digitsOnly.Replace(qtyTopp.Text, "")
    End Sub

    Private Sub qtyTopp_KeyPress(sender As Object, e As KeyPressEventArgs) Handles qtyTopp.KeyPress
        If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        registraTopp.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Dispose()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Dispose()
    End Sub
End Class