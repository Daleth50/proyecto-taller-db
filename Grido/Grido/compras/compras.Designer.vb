﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class compras
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.ComboBox1 = New System.Windows.Forms.ComboBox()
        Me.ProveedoresBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.GridoDataSet = New Grido.GridoDataSet()
        Me.ProveedoresTableAdapter = New Grido.GridoDataSetTableAdapters.ProveedoresTableAdapter()
        Me.btnsabor = New System.Windows.Forms.Button()
        Me.btntopping = New System.Windows.Forms.Button()
        Me.btncompra = New System.Windows.Forms.Button()
        Me.proveedorDG = New System.Windows.Forms.DataGridView()
        Me.NomprovDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TelprovDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.ProveedoresBindingSource1 = New System.Windows.Forms.BindingSource(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        CType(Me.ProveedoresBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridoDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.proveedorDG, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ProveedoresBindingSource1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(288, 23)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(217, 24)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Seleccione el proveedor"
        '
        'ComboBox1
        '
        Me.ComboBox1.DataSource = Me.ProveedoresBindingSource
        Me.ComboBox1.DisplayMember = "nomprov"
        Me.ComboBox1.FormattingEnabled = True
        Me.ComboBox1.Location = New System.Drawing.Point(288, 63)
        Me.ComboBox1.Name = "ComboBox1"
        Me.ComboBox1.Size = New System.Drawing.Size(217, 21)
        Me.ComboBox1.TabIndex = 1
        Me.ComboBox1.ValueMember = "clveprov"
        '
        'ProveedoresBindingSource
        '
        Me.ProveedoresBindingSource.DataMember = "Proveedores"
        Me.ProveedoresBindingSource.DataSource = Me.GridoDataSet
        '
        'GridoDataSet
        '
        Me.GridoDataSet.DataSetName = "GridoDataSet"
        Me.GridoDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'ProveedoresTableAdapter
        '
        Me.ProveedoresTableAdapter.ClearBeforeFill = True
        '
        'btnsabor
        '
        Me.btnsabor.Location = New System.Drawing.Point(49, 136)
        Me.btnsabor.Name = "btnsabor"
        Me.btnsabor.Size = New System.Drawing.Size(147, 49)
        Me.btnsabor.TabIndex = 6
        Me.btnsabor.Text = "Comprar Sabor"
        Me.btnsabor.UseVisualStyleBackColor = True
        '
        'btntopping
        '
        Me.btntopping.Location = New System.Drawing.Point(49, 238)
        Me.btntopping.Name = "btntopping"
        Me.btntopping.Size = New System.Drawing.Size(147, 49)
        Me.btntopping.TabIndex = 7
        Me.btntopping.Text = "Comprar Topping"
        Me.btntopping.UseVisualStyleBackColor = True
        '
        'btncompra
        '
        Me.btncompra.Location = New System.Drawing.Point(405, 335)
        Me.btncompra.Name = "btncompra"
        Me.btncompra.Size = New System.Drawing.Size(147, 49)
        Me.btncompra.TabIndex = 8
        Me.btncompra.Text = "Registrar compra"
        Me.btncompra.UseVisualStyleBackColor = True
        '
        'proveedorDG
        '
        Me.proveedorDG.AllowUserToAddRows = False
        Me.proveedorDG.AllowUserToDeleteRows = False
        Me.proveedorDG.AutoGenerateColumns = False
        Me.proveedorDG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.proveedorDG.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.NomprovDataGridViewTextBoxColumn, Me.TelprovDataGridViewTextBoxColumn})
        Me.proveedorDG.DataSource = Me.ProveedoresBindingSource1
        Me.proveedorDG.Location = New System.Drawing.Point(424, 136)
        Me.proveedorDG.Name = "proveedorDG"
        Me.proveedorDG.ReadOnly = True
        Me.proveedorDG.Size = New System.Drawing.Size(244, 131)
        Me.proveedorDG.TabIndex = 9
        '
        'NomprovDataGridViewTextBoxColumn
        '
        Me.NomprovDataGridViewTextBoxColumn.DataPropertyName = "nomprov"
        Me.NomprovDataGridViewTextBoxColumn.HeaderText = "Nombre"
        Me.NomprovDataGridViewTextBoxColumn.Name = "NomprovDataGridViewTextBoxColumn"
        Me.NomprovDataGridViewTextBoxColumn.ReadOnly = True
        '
        'TelprovDataGridViewTextBoxColumn
        '
        Me.TelprovDataGridViewTextBoxColumn.DataPropertyName = "telprov"
        Me.TelprovDataGridViewTextBoxColumn.HeaderText = "Telefono"
        Me.TelprovDataGridViewTextBoxColumn.Name = "TelprovDataGridViewTextBoxColumn"
        Me.TelprovDataGridViewTextBoxColumn.ReadOnly = True
        '
        'ProveedoresBindingSource1
        '
        Me.ProveedoresBindingSource1.DataMember = "Proveedores"
        Me.ProveedoresBindingSource1.DataSource = Me.GridoDataSet
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(424, 117)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(103, 13)
        Me.Label2.TabIndex = 10
        Me.Label2.Text = "Datos del proveedor"
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(203, 335)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(146, 49)
        Me.Button1.TabIndex = 11
        Me.Button1.Text = "Volver"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'compras
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.proveedorDG)
        Me.Controls.Add(Me.btncompra)
        Me.Controls.Add(Me.btntopping)
        Me.Controls.Add(Me.btnsabor)
        Me.Controls.Add(Me.ComboBox1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "compras"
        Me.Text = "compras"
        CType(Me.ProveedoresBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridoDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.proveedorDG, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ProveedoresBindingSource1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents ComboBox1 As ComboBox
    Friend WithEvents GridoDataSet As GridoDataSet
    Friend WithEvents ProveedoresBindingSource As BindingSource
    Friend WithEvents ProveedoresTableAdapter As GridoDataSetTableAdapters.ProveedoresTableAdapter
    Friend WithEvents btnsabor As Button
    Friend WithEvents btntopping As Button
    Friend WithEvents btncompra As Button
    Friend WithEvents proveedorDG As DataGridView
    Friend WithEvents ProveedoresBindingSource1 As BindingSource
    Friend WithEvents NomprovDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TelprovDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents Label2 As Label
    Friend WithEvents Button1 As Button
End Class
