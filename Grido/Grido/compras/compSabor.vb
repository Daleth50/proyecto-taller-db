﻿Imports System.Text.RegularExpressions

Public Class compSabor
    Dim clveprov = compras.ComboBox1.SelectedValue
    Private Sub compSabor_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'GridoDataSet.Sabor' table. You can move, or remove it, as needed.
        Me.SaborTableAdapter.Fill(Me.GridoDataSet.Sabor)
        Label2.Hide()
        qtySabor.Hide()
        MsgBox(clveprov)
        Try
            Me.SaborTableAdapter.FillBy4(Me.GridoDataSet.Sabor)
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        Label2.Show()
        qtySabor.Show()
    End Sub

    Private Sub qtySabor_TextChanged(sender As Object, e As EventArgs) Handles qtySabor.TextChanged
        Dim digitsOnly As Regex = New Regex("[^\d]")
        qtySabor.Text = digitsOnly.Replace(qtySabor.Text, "")
    End Sub

    Private Sub qtySabor_KeyPress(sender As Object, e As KeyPressEventArgs) Handles qtySabor.KeyPress
        If Not Char.IsNumber(e.KeyChar) AndAlso Not Char.IsControl(e.KeyChar) Then
            e.Handled = True
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Dispose()
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        registraSabor.Show()
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Me.Dispose()
    End Sub
End Class