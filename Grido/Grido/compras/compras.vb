﻿Public Class compras
    Dim clveprov
    Private Sub compras_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: This line of code loads data into the 'GridoDataSet.Proveedores' table. You can move, or remove it, as needed.
        Me.ProveedoresTableAdapter.Fill(Me.GridoDataSet.Proveedores)
        btnsabor.Hide()
        btntopping.Hide()
        proveedorDG.Hide()
        Label2.Hide()
        btncompra.Enabled = False
    End Sub

    Private Sub ComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged
        btncompra.Enabled = True
        clveprov = ComboBox1.SelectedValue()
    End Sub

    Private Sub btncompra_Click(sender As Object, e As EventArgs) Handles btncompra.Click
        btnsabor.Show()
        btntopping.Show()
        btncompra.Hide()
        ComboBox1.Hide()
        Label1.Hide()
        Try
            Me.ProveedoresTableAdapter.FillBy(Me.GridoDataSet.Proveedores, CType(clveprov, Integer))
            Label2.Show()
            proveedorDG.Show()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub btntopping_Click(sender As Object, e As EventArgs) Handles btntopping.Click
        compraTopping.Show()
    End Sub

    Private Sub btnsabor_Click(sender As Object, e As EventArgs) Handles btnsabor.Click
        compSabor.Show()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Dispose()
        mn.Show()
    End Sub
End Class